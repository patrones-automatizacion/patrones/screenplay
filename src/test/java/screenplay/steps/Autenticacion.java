package screenplay.steps;

import co.com.sofka.automation.questions.CompletedLogin;
import co.com.sofka.automation.tasks.GoToLogin;
import co.com.sofka.automation.tasks.OpenTheBrowser;
import co.com.sofka.automation.ui.SouceDemoLoginPage;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.is;

public class Autenticacion {

    private Actor actor;
    private SouceDemoLoginPage souceDemoLoginPage;

    @Before
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
        actor = OnStage.theActorCalled("Usuario");
    }

    @Dado("^un usuario en la pagina inicial de souce demo$")
    public void unUsuarioEnLaPaginaInicialDeSouceDemo() {
        actor.wasAbleTo(
                OpenTheBrowser.on(souceDemoLoginPage)
        );
    }

    @Cuando("^el usuario ingresa un \"([^\"]*)\" y \"([^\"]*)\" correctos$")
    public void elUsuarioIngresaUnYCorrectos(String user, String password) {
        actor.wasAbleTo(
                GoToLogin.toEnter(user, password)
        );
    }

    @Entonces("^se autentica en el sitio correctamente$")
    public void seAutenticaEnElSitioCorrectamente() {
        actor.should(
                seeThat(
                        CompletedLogin.ok(),
                        is(Matchers.equalTo("PRODUCTS"))
                )
        );
    }

}
