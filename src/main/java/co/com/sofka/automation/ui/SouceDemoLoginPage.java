package co.com.sofka.automation.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://www.saucedemo.com/")
public class SouceDemoLoginPage extends PageObject {
    public static final Target USER_INPUT = Target.the("Input usuario").located(By.id("user-name"));
    public static final Target PASSWORD_INPUT = Target.the("Input clave").located(By.id("password"));
    public static final Target LOGIN_BUTTON = Target.the("Botón login").located(By.id("login-button"));
}
