package co.com.sofka.automation.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class SouceDemoHomePage extends PageObject {
    public static final Target HOME_TITLE = Target.the("Titulo home").locatedBy("//*[@class='title']");
}
