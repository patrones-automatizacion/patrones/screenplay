package co.com.sofka.automation.tasks;

import co.com.sofka.automation.ui.SouceDemoLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GoToLogin implements Task {

    private String user;
    private String password;

    public GoToLogin(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(user).into(SouceDemoLoginPage.USER_INPUT),
                Enter.theValue(password).into(SouceDemoLoginPage.PASSWORD_INPUT),
                Click.on(SouceDemoLoginPage.LOGIN_BUTTON)
        );

    }

    public static GoToLogin toEnter(String user, String password) {
        return instrumented(GoToLogin.class, user, password);
    }

}
